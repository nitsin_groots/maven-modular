#!/bin/bash
#Get servers list
set -fe
string=$JUMP_SERVER
array=(${string//,/ })
for i in "${!array[@]}"
do    
      echo "Deploy project on server ${array[i]}"    
      #ssh ubuntu@${array[i]} "sudo mkdir /uat" 
      #ssh ubuntu@${array[i]} "sudo chown -R ubuntu:ubuntu /uat"
      #scp ./target/sentry-spring-boot-log4j2-example-0.0.1.jar ubuntu@${array[i]}:/uat/artifacts/
      scp -r ./module1 ubuntu@${array[i]}:/home/ubuntu/
      scp -r ./module1/target/ ubuntu@${array[i]}:/home/ubuntu/test1/
      scp -r ./module2/target/ ubuntu@${array[i]}:/home/ubuntu/test2/
      #ssh ubuntu@${array[i]} "rm -r /home/ubuntu/test/artifacts/*"
      #ssh ubuntu@${array[i]} "cp -r /home/ubuntu/test/target/* /home/ubuntu/test/artifacts/"
      #ssh ubuntu@${array[i]} "rm -rf /home/ubuntu/test/target"
      #ssh ubuntu@${array[i]} "/bin/bash /uat/app_deploy.sh"
done
